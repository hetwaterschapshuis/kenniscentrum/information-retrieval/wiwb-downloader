from setuptools import setup, find_packages

setup(
    name='wiwb-downloader',
    version='1.1.0',
    description='Package to download data from the WIWB',
    classifiers=[
        "Programming Language :: Python :: 3.8"],
    keywords='api wiwb waterschap knmi',
    url='https://gitlab.com/hetwaterschapshuis/kenniscentrum/information-retrieval/wiwb-downloader',
    author='Tobias Cadée',
    author_email='tcadee@hhsk.nl',
    license='MIT',
    packages=find_packages(),
    install_requires=['tqdm','requests'],
    include_package_data=True,
    zip_safe=False
    # entry_points={
    #     'console_scripts': [
    #         'wiwb_download = wiwb_downloader.download:main'
    #         'print_wiwb_config = wiwb_downloader.config:print_config'
    #     ]}
    )
