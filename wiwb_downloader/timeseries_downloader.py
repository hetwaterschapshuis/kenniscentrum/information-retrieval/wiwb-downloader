import pandas as pd
from shapely.geometry import Point
import logging


from .config import CONFIG
from .downloader import Downloader


class TimeSeriesDownloader(Downloader):
    """Class to download timeseries data from WIWB.

    Extends the Download class.

    Attributes
    ----------
    variables : list
        list of variables to download.
    step : timedelta
        Data source frequency.
    status_bar : tqdm
        Progress bar.
    shape : bool/MultiPolygon
        Shape from shapefile or False if none provided.
    extent : list
        Extent coordinates.
    """

    def __init__(self, data_source, extent, type, start_date, end_date, args, username='WIWB_USERNAME', password='WIWB_PASSWORD'):
        super().__init__(data_source, type, start_date, end_date, username, password)
        self.variables = args['Variables']
        self.step = self.parse_timedelta(
            args.get("Step", CONFIG[data_source]['Step']))
        self.interval = args.get('Interval', 0)
        self.shape, self.extent = self.get_extent(extent)

    def __get_body(self):
        """Get body for download request.

        Returns
        -------
        dict
            Request body.
        """
        start_date = self.datetimeToString(self.start_date)
        end_date = self.datetimeToString(self.end_date)
        body = {
            "Readers": [{
                "DataSourceCode": self.data_source_id,
                "Settings": {
                    "StartDate": start_date,
                    "EndDate": end_date,
                    "VariableCodes": self.variables,
                    "Extent": {
                        "Xll": self.extent[0],
                        "Yll": self.extent[1],
                        "Xur": self.extent[2],
                        "Yur": self.extent[3],
                        "SpatialReference": {
                            "Epsg": 28992
                        }
                    }
                }
            }],
            "Exporter": {
                "DataFormatCode": "json",
                "Settings": {
                    "Formatting": "Indented"
                }
            }
        }
        if self.interval != 0:
            body['Readers'][0]['Settings']['Interval'] = {
                "Type": self.interval['Type'], "Value": self.interval["Value"]}
        return body

    def __get_locations_to_keep(self, all_locations):
        """Get locations within shapefile if self.shape exists.

        Parameters
        ----------
        all_locations : dict
            Locations with coordinates

        Returns
        -------
        list
            Locations to download.
        """
        if self.shape:
            points = {code: Point(value['X'], value['Y'])
                      for code, value in all_locations.items()}
            locations = [code for code, point in points.items()
                         if self.shape.contains(point)]
        else:
            locations = all_locations.keys()
        return locations

    def __parse_json(self, content):
        """Parse json content into DataFrame

        Parameters
        ----------
        content : dict
            Json content from download.

        Returns
        -------
        DataFrame
            DataFrame containing data.
        """
        if 'Exception' in content:
            output_df = pd.DataFrame()
        else:
            locations = self.__get_locations_to_keep(
                content['Meta']['Locations'])
            output_df = pd.DataFrame()
            for ts in content['Data']:
                if ts['LocationCode'] in locations:
                    ts_dict = {}
                    for value in ts['Data']:
                        ts_dict[self.stringToDateTime(
                            value['DateTime'])] = value['Value']
                    if all([element == -9999 for element in ts_dict.values()]):
                        continue
                    ts_series = pd.Series(ts_dict, name=ts['LocationCode'])
                    output_df[ts['LocationCode']] = ts_series
            output_df.index.name = 'StartDate'
        return output_df

    def download_df(self):
        """Download data for this data source.
        """
        body = self.__get_body()
        logging.info('Downloading data')
        content = self.make_request(body)
        if content == False:
            return pd.DataFrame()
        logging.info('Parsing data')
        output_df = self.__parse_json(content)
        return output_df
