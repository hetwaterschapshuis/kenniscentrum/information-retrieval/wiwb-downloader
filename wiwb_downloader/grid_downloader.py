import numpy as np
import pandas as pd
import logging
from .config import CONFIG
from .downloader import Downloader


class GridDownloader(Downloader):
    """Class to download grid data from WIWB.

    Extends the Download class.

    Attributes
    ----------
    variables : list
        list of variables to download.
    step : timedelta
        Data source frequency.
    status_bar : tqdm
        Progress bar.
    shape : bool/MultiPolygon
        Shape from shapefile or False if none provided.
    extent : list
        Extent coordinates.
    epsg : string
        Epsg code for downloaded grids.
    """

    def __init__(self, data_source, extent, type, start_date, end_date, args, username='WIWB_USERNAME', password='WIWB_PASSWORD'):
        super().__init__(data_source, type, start_date, end_date, username, password)
        self.variables = args['Variables']
        self.interval = args.get('Interval', 0)
        self.step = self.parse_timedelta(CONFIG[data_source]['Step'])
        self.epsg = CONFIG[data_source]['EPSG']
        self.shape, self.extent = self.get_extent(extent)

    def __get_body(self):
        """Get body for download request.

        Returns
        -------
        dict
            Request body.
        """
        start_date = self.datetimeToString(self.start_date)
        end_date = self.datetimeToString(self.end_date)
        body = {
            "Readers": [{
                "DataSourceCode": self.data_source_id,
                "Settings": {
                    "StartDate": start_date,
                    "EndDate": end_date,
                    "VariableCodes": self.variables,
                    "Extent": {
                        "Xll": self.extent[0],
                        "Yll": self.extent[1],
                        "Xur": self.extent[2],
                        "Yur": self.extent[3],
                        "SpatialReference": {
                            "Epsg": 28992
                        }
                    }
                }
            }],
            "Exporter": {
                "DataFormatCode": "json",
                "Settings": {
                    "Formatting": "Indented"
                }
            }
        }
        if self.interval != 0:
            body['Readers'][0]['Settings']['Interval'] = {"Type": self.interval["Type"], "Value": self.interval["Value"]}
        return body

    # @Timer(name="decorator", text="parsing time: {milliseconds:.0f} ms")
    def __parse_json(self, content):
        """Parse json content into dictionary

        Parameters
        ----------
        content : dict
            Json content from download.

        Returns
        -------
        dict
            Dictionary containing downloaded data.
        """
        output_dict = {}
        self.grid_def = content['Data'][0]['GridDefinition']
        self.cells = self.get_cells_to_keep()
        self.coordinates = {key: (value.centroid.x,value.centroid.y) for key, value in self.cells.items()}
        for grid in content['Data']:
            if self.shape:
                data = np.array(grid['Data'])[list(self.cells.keys())].tolist()
            else:
                data = np.array(grid['Data']).tolist()
            output_dict[self.counter] = [self.stringToDateTime(grid['StartDate']),
                                         grid['VariableCode']] + data
            self.counter += 1
        return output_dict

    def download_df(self):
        """Download data for this data source.
        """
        body = self.__get_body()
        logging.info('Downloading data')
        content = self.make_request(body)
        if content == False:
            return pd.DataFrame()
        logging.info('Parsing data')
        output_dict = self.__parse_json(content)
        columns = ['StartDate', 'Variable'] + list(self.cells.keys())
        output_df = pd.DataFrame.from_dict(output_dict, orient='index', columns=columns)
        return output_df
