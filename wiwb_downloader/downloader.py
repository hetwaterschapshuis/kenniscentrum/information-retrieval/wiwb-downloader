import os
from datetime import datetime, timedelta
from dateutil.parser import parse
from math import floor
import geopandas as gpd
import requests
from requests.auth import HTTPBasicAuth
from shapely.geometry import Polygon
from pyproj import Transformer, CRS
from .config import CONFIG
import logging

class Downloader():
    """Base class for downloading WIWB data.


    This class contains the most important methods for downloading data.

    Attributes
    ----------
    data_source : string
        The data source name.
    start_date : datetime
        Start date for download request.
    end_date : datetime
        End date for download request.
    header : dict
        Header for download request.
    auth : dict
        Authentication for download request.
    url : string
        API url.
    data_source_id : string
        Data source id.
    counter : int
        Counter for progress bar.
    data_path : Path to download data to.
    """

    def __init__(self, data_source, type, start_date, end_date, username, password):
        self.data_source = data_source
        if isinstance(start_date, str):
            self.end_date = parse(end_date)
            self.start_date = parse(start_date)
        else:
            self.end_date = end_date
            self.start_date = start_date
        self.header = self.__get_header()
        self.auth = self.__get_authentication(username, password)
        self.url = f"https://wiwb.hydronet.com/api/{type}/get"
        self.data_source_id = CONFIG[data_source]['DataSourceID']
        self.counter = 0
        self.data_path = ""

    def __get_header(self):
        """Get header for download request

        Returns
        -------
        dict
            Header for download request
        """
        header = {
            "Accept": "image/tiff",
            "Content-Type": "application/json"
        }
        return header

    def __get_authentication(self, username, password):
        """Get authentication for download request.

        Returns
        -------
        HTTPBasicAuth
            Authentication token
        """
        username = os.environ[username]
        password = os.environ[password]
        auth = HTTPBasicAuth(username, password)
        return auth

    def parse_timedelta(self, step):
        """Parse timedelta string.

        Parameters
        ----------
        step : string
            string containing timedelta.

        Returns
        -------
        timedelta
            parsed timedelta.
        """
        quantity = int(step[:-1])
        value = step[-1]
        if value == 'm':
            return timedelta(minutes=quantity)
        elif value == 'h':
            return timedelta(hours=quantity)
        elif value == 'd':
            return timedelta(days=quantity)

    def datetimeToString(self, date):
        """Convert datetime to string.

        Parameters
        ----------
        date : datetime
            datetime to convert.

        Returns
        -------
        string
            converted datetime in string format.
        """
        return date.strftime("%Y%m%d%H%M%S")

    def stringToDateTime(self, date):
        """Convert string to datetime.

        Parameters
        ----------
        date : string
            string to convert.

        Returns
        -------
        datetime
            converted string in datetime format.
        """
        return datetime.strptime(date, "%Y%m%d%H%M%S")

    # @Timer(name="decorator", text="get_extent time: {milliseconds:.0f} ms")
    def get_extent(self, extent):
        """Get extent from extent argument.

        If extent is shapefile path, get the total_bounds from this shapefile.

        Parameters
        ----------
        extent : list/string/shapely Polygon
            extent for download request

        Returns
        -------
        (MultiPolygon/False, list)
            Shape if extent is shapefile path, or False and extent.
        """
        if isinstance(extent, list):
            shape = False
            extent = extent
        else:
            if isinstance(extent, Polygon):
                self.shape_df = gpd.GeoDataFrame(geometry = [extent])\
                    .set_crs(epsg=28992)
            elif isinstance(extent, str):
                self.shape_df = gpd.read_file(extent).set_crs(epsg=28992)
            else:
                raise ValueError (
                    "type of extent is not recognized"
                )
            shape = self.shape_df.unary_union
            extent = self.shape_df.total_bounds
        return shape, extent

    # @Timer(name="decorator", text="get_cells_to_keep time: {milliseconds:.0f} ms")
    def get_cells_to_keep(self):
        """Check which gridcells fall within the shapefile.

        Return all cells if no shapefile is provided.


        Returns
        -------
        dict
            dict with cell index and coordinates.
        """
        rows, cols = self.grid_def['Rows'], self.grid_def['Columns']
        cellwidth = self.grid_def['CellWidth']
        cellheight = self.grid_def['CellHeight']
        xll = self.grid_def['Xll']
        yll = self.grid_def['Yll']
        ncells = rows * cols
        if self.epsg == 10000:
            in_crs = CRS.from_proj4(
                "+proj=stere +lat_0=90 +lon_0=0 +lat_ts=60 +x_0=0 +y_0=0 +k=1 +a=6378140 +b=6356750 +x_0=0 y_0=0")
            factor = 1000
        else:
            factor = 1
            in_crs = CRS.from_epsg(self.epsg)
        transformer = Transformer.from_crs(in_crs, CRS.from_epsg(28992))
        grid_dict = {}
        for i in range(ncells):
            col = i % cols
            row = floor(i / cols)
            xmin = (xll + (col * cellwidth)) * factor
            ymin = (yll + ((rows - row) * cellheight)) * factor
            xmax = xmin + (cellwidth * factor)
            ymax = ymin + (cellheight * factor)
            xmin, ymin = transformer.transform(xmin, ymin)
            xmax, ymax = transformer.transform(xmax, ymax)
            grid_dict[i]=Polygon([(xmin, ymin), (xmax, ymin), (xmax, ymax), (xmin, ymax)])
        if self.shape:
            grid_dict={i: cell for i, cell in grid_dict.items() if self.shape.intersects(cell)}
        return grid_dict

    # @Timer(name="decorator", text="make_request timxe: {milliseconds:.0f} ms")
    def make_request(self, body):
        """Make API request and return content.

        Parameters
        ----------
        body : dict
            Request body.

        Returns
        -------
        dict
            json content from request.
        """
        response=requests.post(
            url = self.url,
            auth = self.auth,
            headers = self.header,
            json = body
        )
        response.encoding='utf-8-sig'
        try:
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as e:
            self.response_text = e
            logging.info(e)
            logging.info(f"error occured for following body: {body}")
            return False

    def download(self, return_df):
        """Download data for this data source.
        """
        output_df=self.download_df()
        if return_df:
            return output_df
        else:
            self.save_output(output_df)

    def save_output(self, output_df):
        """Save output to csv file.

        Parameters
        ----------
        output_df : DataFrame
            DataFrame containing downloaded data.
        """
        file_out=f'{self.data_source.lower().replace(" ","_")}.csv'
        output_df.to_csv(os.path.join(self.data_path, file_out))
