# WIWB DOWNLOADER

WIWB Downloader is a python package to make downloading from the WIWB API easy to use.
The code is open source and suggestions are welcome.

## 0.5.0 Release

Missed a few releases in the readme, so will update soon.

## 0.3.6 Release

It is now easier to download from within Python. You can use the multi_download function to download data from the WIWB based on a parameter json file or a dict with the same properties.
This returns a dict with the name of the data source and a dataframe containing the data.

## 0.3.5 Release

You can now customise the wiwb username and password environment variables. When creating a downloader class instance, you need to add this at the end of the arguments like this:
downloader = ModelGridDownloader(data_source, extent, type, start_date, args, username, password)
the default is WIWB_USERNAME and WIWB_PASSWORD.

Also, grid_def is now a attribure of the Download class and all inherited subclasses. 

## 0.3.2 Release

You can now print the config for data sources. Here you can look up the type, datasourceid, step, and start date for each data source. To print the config in the command line, use print_wiwb_config. From within python, use: wiwb_downloader.print_config().

Progress bars are removed for GridDownloader and Timeseries downloader as the data is retrieved in 1 requests so no progress can be shown. You now get printed updates.

## Features

- Download Grid data
- Download ModelGrid data
- Download TimeSeries dat
- Use shapefile to get data only for your area of interest.
- Export data to csv.
- Command line usage

## Installation

To install, first create an environment with geopandas. Install this via conda: conda install geopandas.
```

You can then install via pip:
```sh
pip install git+https://gitlab.com/hetwaterschapshuis/kenniscentrum/information-retrieval/wiwb-downloader
```

Furthermore, if you want to use the WIWB-API your IP adress needs to be whitelisted. You can contact Het Waterschapshuis (HWH) for access.

# Usage

### Username and password
The first thing to be able to download data from the WIWB API is to set your username and password.
These need to be set in your environment variables:
username: _WIWB_USERNAME_ 
password: _WIWB_PASSWORD_

### Parameter file
To download data you need a parameters json file to specify your download request.
This file contains the following structure:
```json
{
    "StartDate": In any well known datetime format, eg: YYYY:MM:DD,
    "EndDate": In any well known datetime format, eg: YYYY:MM:DD,
    "Extent": Either list with coordinates in EPSG 28992 or path to shapefile,
    "DataPath" : Optional, if you want to save your data in a specific location,
    "DataSources": {
        DATA_SOURCE_NAME : {
            "Variables": List of variables to download for DATA_SOURCE_NAME,
            "TimeAhead": Only used when requesting modelgrids, explanation can be found below.
            "Offset": Only used when requesting modelgrids, explanation can be found below.
            "Interval": Only used when requesting grids or timeseries. You can find an example in test.json
                https://gitlab.com/tcadee/wiwb_downloader/-/blob/master/examples/test.json
        }
        DATA_SOURCE_NAME : {
            you can add as many data sources as you would like.
        }
    }
}
```
Extent can either be a path to a shapefile or a list with coordinates. The format for this coordinate list has to be [xll, yll, xur, lur] (lowerleft and upright). This will function as a bouding box for your request.

DATA_SOURCE_NAME has to be the same name as provided in the WIWB technical instructions. 
The same holds for the variable names.

TimeAhead and Offset are used for model grids. For example: The Harmonie model returns 48 hour predictions and is generated every 6 hours. 
If you only want to retrieve data from 6-12 hours for every model you need to set Offset to '6h' and TimeAhead to '12h'. 
For minutes use '#m' and for days use '#d'.

For parameter.json examples: 
https://gitlab.com/tcadee/wiwb_downloader/-/tree/master/examples

For Data Source names and variables:
https://portal.hydronet.com/data/files/Technische%20Instructies%20WIWB%20API.pdf

### Download and save data

To download, run the following command in your command prompt:
```sh
wiwb_download [parameter_file]
```

### Use inside python script (or notebook)

To use this package inside your python script, all you need to do is import the right classes to be able to download data.
There are three types of data download classes:
- GridDownloader
- ModelGridDownloader
- TimeSeriesDownloader

To use, first import like this:
from wiwb_downloader import ModelGridDownloader

after this, you need to initialize this class with the right arguments(These can also be found in the docstrings):
```sh
downloader = ModelGridDownloader(data_source, extent, type, start_date, args)
```
Most of these arguments are the same as used in the parameters.json file specified earlier. The data_source is DATA_SOURCE_NAME.
args is the dictionary that you fill in under the DATA_SOURCE_NAME. So in any case you need {'Variables':[list of variables]} and in the modelgriddownloader case you can add offset and timeahead. Earlier in this document you can find the exact names of the keys for this dictionary. 

After initializing the class instance, you can download data, this is as easy as:
```sh
df = downloader.download(return_df = True)
```
return_df = True makes sure that the downloaded DataFrame does not get saved to dict but instead returned, so you can use it inside your python script if you want. If you just want to save the data, you can leave it out.

If you want to change the path where your data is downloaded, use:
```sh
downloader.data_path = "put path here"
```
s
The script is equipped with progress bars to monitor your progress.

## License

MIT
